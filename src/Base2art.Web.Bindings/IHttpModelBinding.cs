﻿namespace Base2art.Web.Bindings
{
    using System.Threading.Tasks;
    using Http;

    public interface IHttpModelBinding
    {
        Task<object> BindModelAsync(IHttpRequest request);
    }

    public interface IHttpModelBinding<T> : IHttpModelBinding
        where T : class
    {
        new Task<T> BindModelAsync(IHttpRequest request);
    }
}