﻿namespace Base2art.Web.Bindings
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IModelBinding
    {
        Task<object> BindModelAsync(Uri uri, IReadOnlyDictionary<string, string> cookies, IReadOnlyDictionary<string, string> headers);
    }

    public interface IModelBinding<T> : IModelBinding
        where T : class
    {
        new Task<T> BindModelAsync(Uri uri, IReadOnlyDictionary<string, string> cookies, IReadOnlyDictionary<string, string> headers);
    }
}