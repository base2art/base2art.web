namespace Base2art.Web.Bindings
{
    using System.Threading.Tasks;
    using Http;

    public interface IResponseEditingHttpModelBinding
    {
        Task<object> BindModelAsync(IHttpRequest request, IHttpResponse response);
    }

    public interface IResponseEditingHttpModelBinding<T> : IResponseEditingHttpModelBinding
        where T : class
    {
        new Task<T> BindModelAsync(IHttpRequest request, IHttpResponse response);
    }
}