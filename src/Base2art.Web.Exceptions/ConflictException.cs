﻿namespace Base2art.Web.Exceptions
{
    using System;

    public class ConflictException : Exception
    {
        public ConflictException() : this("Operation would result in an invalid state")
        {
        }

        public ConflictException(string message) : base(message)
        {
        }

        public ConflictException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}