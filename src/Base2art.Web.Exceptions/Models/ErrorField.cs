namespace Base2art.Web.Exceptions.Models
{
    public class ErrorField
    {
        public string Message { get; set; }

        public string Code { get; set; }

        public string Path { get; set; }
    }
}