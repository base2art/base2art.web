﻿namespace Base2art.Web.Exceptions
{
    using System;

    public class NotAuthenticatedException : Exception
    {
        public NotAuthenticatedException() : this("You are not authenticated. Or are using an incorrect authenication mechanism.")
        {
        }

        public NotAuthenticatedException(string message) : base(message)
        {
        }

        public NotAuthenticatedException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}