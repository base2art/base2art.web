﻿namespace Base2art.Web.Exceptions
{
    using System;

    public class NotAuthorizedToResourceException : Exception
    {
        public NotAuthorizedToResourceException() : this("Incorrect authorization")
        {
        }

        public NotAuthorizedToResourceException(string message) : base(message)
        {
        }

        public NotAuthorizedToResourceException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}