﻿namespace Base2art.Web.Exceptions
{
    using System;

    public class NotFoundException : Exception
    {
        public NotFoundException() : this("Resource or Endpoint not found")
        {
        }

        public NotFoundException(string message) : base(message)
        {
        }

        public NotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}