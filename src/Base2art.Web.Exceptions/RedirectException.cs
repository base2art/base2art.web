﻿﻿namespace Base2art.Web.Exceptions
{
    using System;

    public class RedirectException : Exception
    {
        public string Location { get; }
        public bool IsPermanent { get; }
        public bool PreserveMethod { get; }

        public RedirectException() : this("/", true, false, "Resource or Endpoint is located in a new or canonical location")
        {
        }

        public RedirectException(string location, bool isPermanent)
            : this(location, isPermanent, false, "Resource or Endpoint is located in a new or canonical location")
        {
        }

        public RedirectException(string location, bool isPermanent, bool preserveMethod)
            : this(location, isPermanent, preserveMethod, "Resource or Endpoint is located in a new or canonical location")
        {
        }

        public RedirectException(string location, bool isPermanent, string message) 
            : this(location, isPermanent, false, message)
        {
        }

        public RedirectException(string location, bool isPermanent, bool preserveMethod, string message) : base(message)
        {
            this.Location = location;
            this.IsPermanent = isPermanent;
            this.PreserveMethod = preserveMethod;
        }

        public RedirectException(string location, bool isPermanent, string message, Exception innerException)
            : this(location, isPermanent, false, message, innerException)
        {
        }

        public RedirectException(string location, bool isPermanent, bool preserveMethod, string message, Exception innerException) : base(message, innerException)
        {
            this.Location = location;
            this.IsPermanent = isPermanent;
            this.PreserveMethod = preserveMethod;
        }
    }
}