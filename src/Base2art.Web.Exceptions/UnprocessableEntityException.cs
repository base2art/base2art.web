﻿namespace Base2art.Web.Exceptions
{
    using System;
    using Models;

    public class UnprocessableEntityException : Exception
    {
        [Obsolete("Use The Errors ctor", true)]
        public UnprocessableEntityException() => this.Errors = new[]
                                                               {
                                                                   new ErrorField
                                                                   {
                                                                       Code = "UNKNOWN",
                                                                       Message = "UNKNOWN",
                                                                       Path = "UNKNOWN"
                                                                   }
                                                               };

        public UnprocessableEntityException(ErrorField[] fields) => this.Errors = fields;

        public UnprocessableEntityException(string message, ErrorField[] fields) : base(message) => this.Errors = fields;

        public UnprocessableEntityException(string message, ErrorField[] fields, Exception innerException) : base(message, innerException) =>
            this.Errors = fields;

        public ErrorField[] Errors { get; }
    }
}