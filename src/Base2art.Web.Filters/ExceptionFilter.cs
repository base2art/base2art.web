﻿namespace Base2art.Web.Filters
{
    using System;

    public static class ExceptionFilter
    {
        public static void SuppressLogging(this Exception exception)
        {
            if (exception?.Data != null)
            {
                exception.Data["Base2art.Logged"] = true;
            }
        }

        public static bool ShouldLog(this Exception exception)
            => exception?.Data?.Contains("Base2art.Logged") != true;
    }
}