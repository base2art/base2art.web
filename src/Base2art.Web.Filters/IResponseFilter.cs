namespace Base2art.Web.Filters
{
    using Web.Http;

    public interface IResponseFilter
    {
        void Filter(object content, IHttpRequest request, IHttpResponse response);
    }

    public interface IResponseFilter<in T> : IResponseFilter
    {
        void Filter(T content, IHttpRequest request, IHttpResponse response);
    }
}