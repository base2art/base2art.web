﻿namespace Base2art.Web.Filters.Specialized
{
    using System;
    using System.Net;
    using Http;
    using Web.Filters;

    public abstract class RedirectFilter<T> : IResponseFilter<T>
    {
        void IResponseFilter<T>.Filter(T content, IHttpRequest request, IHttpResponse response)
            => this.SetRedirectInternal(content, request, response);

        void IResponseFilter.Filter(object content, IHttpRequest request, IHttpResponse response)
            => this.SetRedirectInternal((T) content, request, response);

        protected abstract void SetRedirect(T content, Action<HttpStatusCode, string> setRedirect);

        private void SetRedirectInternal(T content, IHttpRequest request, IHttpResponse response)
        {
            HttpStatusCode? statusCode = null;
            string location = null;

            this.SetRedirect(content, (x, y) =>
            {
                statusCode = x;
                location = y;
            });

            if (!string.IsNullOrWhiteSpace(location))
            {
                var inUri = request.Uri;
                var outUri = new Uri(inUri, location);
                response.Headers.Add("Location", outUri.ToString());
            }

            if (statusCode != null)
            {
                response.StatusCode = (int) statusCode.Value;
            }
        }
    }
}