﻿namespace Base2art.Web.Filters.Specialized
{
    using System.Collections.Generic;
    using System.Net;
    using Http;
    using Web.Filters;

    public abstract class SetCookieFilter<T> : IResponseFilter<T>
    {
        void IResponseFilter<T>.Filter(T content, IHttpRequest request, IHttpResponse response)
            => this.SetCookiesInternal(content, response.Cookies);

        void IResponseFilter.Filter(object content, IHttpRequest request, IHttpResponse response)
            => this.SetCookiesInternal((T) content, response.Cookies);

        protected abstract void SetCookies(T content, IList<Cookie> responseCookies);

        private void SetCookiesInternal(T content, IHttpResponseCookies responseCookies)
        {
            var cookies = new List<Cookie>();
            this.SetCookies(content, cookies);

            foreach (var cookie in cookies)
            {
                var cookieOptions = new CookieOptions
                                    {
                                        Expires = cookie.Expires,
                                        HttpOnly = cookie.HttpOnly,
                                        Secure = cookie.Secure
                                    };

                if (!string.IsNullOrWhiteSpace(cookie.Domain))
                {
                    cookieOptions.Domain = cookie.Domain;
                }

                cookieOptions.Path = cookie.Path;
                if (string.IsNullOrWhiteSpace(cookie.Path))
                {
                    cookieOptions.Path = "/";
                }

                responseCookies.Append(cookie.Name, cookie.Value, cookieOptions);
            }
        }
    }
}