﻿namespace Base2art.Web.Filters.Specialized
{
    using System.Collections.Generic;
    using Http;
    using Web.Filters;

    public abstract class SetHeadersFilter<T> : IResponseFilter<T>
    {
        void IResponseFilter<T>.Filter(T content, IHttpRequest request, IHttpResponse response)
            => this.SetHeaders(content, response.Headers);

        void IResponseFilter.Filter(object content, IHttpRequest request, IHttpResponse response)
            => this.SetHeaders((T) content, response.Headers);

        protected abstract void SetHeaders(T content, ICollection<KeyValuePair<string, string>> headers);
    }
}