﻿namespace Base2art.Web.Filters.Specialized
{
    using System;
    using System.Net;
    using Http;
    using Web.Filters;

    public abstract class SetStatusCodeFilter<T> : IResponseFilter<T>
    {
        void IResponseFilter<T>.Filter(T content, IHttpRequest request, IHttpResponse response)
            => this.SetStatusCode(content, x => response.StatusCode = (int) x);

        void IResponseFilter.Filter(object content, IHttpRequest request, IHttpResponse response)
            => this.SetStatusCode((T) content, x => response.StatusCode = (int) x);

        protected abstract void SetStatusCode(T content, Action<HttpStatusCode> setValue);
    }
}