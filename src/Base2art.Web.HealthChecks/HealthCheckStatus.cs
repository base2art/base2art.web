﻿namespace Base2art.Web.HealthChecks
{
    using System;

    public class HealthCheckStatus
    {
        public bool IsUnhealthy { get; set; }
        public Exception Exception { get; set; }
        public string Message { get; set; }
    }
}