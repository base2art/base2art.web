﻿namespace Base2art.Web.HealthChecks
{
    using System.Threading.Tasks;

    public interface IHealthCheck
    {
        Task<HealthCheckStatus> ExecuteAsync();
    }
}