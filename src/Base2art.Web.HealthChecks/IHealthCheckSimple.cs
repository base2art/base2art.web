﻿namespace Base2art.Web.HealthChecks
{
    using System.Threading.Tasks;

    public interface IHealthCheckSimple
    {
        Task ExecuteAsync();
    }
}