﻿namespace Base2art.Web.Http
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    public interface IHttpRequest
    {
        /// <summary>
        /// Gets or sets the HTTP method.
        /// </summary>
        /// <returns>The HTTP method.</returns>
        string Method { get; }

        /// <summary>
        /// Gets or sets the Host header. May include the port.
        /// </summary>
        /// <return>The Host header.</return>
        Uri Uri { get; }

        /// <summary>
        /// Gets the request headers.
        /// </summary>
        /// <returns>The request headers.</returns>
        IReadOnlyDictionary<string, string> Headers { get; }

        /// <summary>
        /// Gets the collection of Cookies for this request.
        /// </summary>
        /// <returns>The collection of Cookies for this request.</returns>
        IReadOnlyDictionary<string, string> Cookies { get; }

        //        /// <summary>
        //        /// Gets or sets the Content-Length header.
        //        /// </summary>
        //        /// <returns>The value of the Content-Length header, if any.</returns>
        //        public abstract long? ContentLength { get; set; }

//        /// <summary>
//        /// Gets or sets the Content-Type header.
//        /// </summary>
//        /// <returns>The Content-Type header.</returns>
//        string ContentType { get; set; }

//        /// <summary>
//        /// Gets or sets the RequestBody Stream.
//        /// </summary>
//        /// <returns>The RequestBody Stream.</returns>
//        public abstract Stream Body { get; set; }

//        /// <summary>
//        /// Checks the Content-Type header for form types.
//        /// </summary>
//        /// <returns>true if the Content-Type header represents a form content type; otherwise, false.</returns>
//        public abstract bool HasFormContentType { get; }

//        /// <summary>
//        /// Gets or sets the request body as a form.
//        /// </summary>
//        public abstract IFormCollection Form
    }
}