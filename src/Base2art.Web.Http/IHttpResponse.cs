﻿namespace Base2art.Web.Http
{
    using System.Collections.Generic;
    using System.IO;

    public interface IHttpResponse
    {
        /// <summary>Gets or sets the HTTP response code.</summary>
        int StatusCode { get; set; }

        /// <summary>Gets the response headers.</summary>
        IDictionary<string, string> Headers { get; }

        /// <summary>
        ///     Gets or sets the response body <see cref="T:System.IO.Stream" />.
        /// </summary>
        Stream Body { get; set; }

        /// <summary>
        ///     Gets or sets the value for the <c>Content-Length</c> response header.
        /// </summary>
        long? ContentLength { get; set; }

        /// <summary>
        ///     Gets or sets the value for the <c>Content-Type</c> response header.
        /// </summary>
        string ContentType { get; set; }

        /// <summary>
        ///     Gets an object that can be used to manage cookies for this response.
        /// </summary>
        IHttpResponseCookies Cookies { get; }

        /// <summary>
        ///     Gets a value indicating whether response headers have been sent to the client.
        /// </summary>
        bool HasStarted { get; }

        /// <summary>
        ///     Returns a temporary redirect response (HTTP 302) to the client.
        /// </summary>
        /// <param name="location">The URL to redirect the client to.</param>
        void Redirect(string location);

        /// <summary>
        ///     Returns a redirect response (HTTP 301 or HTTP 302) to the client.
        /// </summary>
        /// <param name="location">The URL to redirect the client to.</param>
        /// <param name="permanent"><c>True</c> if the redirect is permanent (301), otherwise <c>false</c> (302).</param>
        void Redirect(string location, bool permanent);
    }
}