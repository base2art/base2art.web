namespace Base2art.Web.Messages
{
    using System.Collections.Generic;
    using MessageQueue;

    public class TaskExecutionMessage : MessageBase
    {
        public string TaskName { get; set; }

        public Dictionary<string, object> Data { get; set; }
    }
}