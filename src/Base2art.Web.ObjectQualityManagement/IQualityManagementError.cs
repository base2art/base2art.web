namespace Base2art.Web.ObjectQualityManagement
{
    public interface IQualityManagementError
    {
        string ErrorCode { get; }
        string ErrorMessage { get; }
        string PropertyName { get; }
    }
}