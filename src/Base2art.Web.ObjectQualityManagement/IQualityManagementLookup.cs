namespace Base2art.Web.ObjectQualityManagement
{
    using System.Collections.Generic;

    public interface IQualityManagementLookup
    {
        IEnumerable<IValidator<T>> GetValidators<T>();
        IEnumerable<IVerifier<T>> GetVerifiers<T>();
    }
}