namespace Base2art.Web.ObjectQualityManagement
{
    public interface IQualityManagementResult
    {
        bool IsValid { get; }
        IQualityManagementError[] Errors { get; }
    }
}