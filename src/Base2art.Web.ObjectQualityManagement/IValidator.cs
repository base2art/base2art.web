namespace Base2art.Web.ObjectQualityManagement
{
    public interface IValidator<in T>
    {
        IQualityManagementResult Validate(T data);
    }
}