namespace Base2art.Web.ObjectQualityManagement
{
    using System.Threading.Tasks;

    public interface IVerifier<in T>
    {
        Task<IQualityManagementResult> Verify(T data);
    }
}