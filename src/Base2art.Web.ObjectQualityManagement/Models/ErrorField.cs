namespace Base2art.Web.ObjectQualityManagement.Models
{
    public class ErrorField
    {
        public string Message { get; set; }

        public string Code { get; set; }

        public string Path { get; set; }
    }
}