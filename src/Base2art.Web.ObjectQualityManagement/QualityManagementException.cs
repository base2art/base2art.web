﻿namespace Base2art.Web.ObjectQualityManagement
{
    using System;
    using Models;

    public class QualityManagementException : Exception
    {
        [Obsolete("Use The Errors ctor", false)]
        protected QualityManagementException() => this.Errors = new[]
                                                                      {
                                                                          new ErrorField
                                                                          {
                                                                              Code = "UNKNOWN",
                                                                              Message = "UNKNOWN",
                                                                              Path = "UNKNOWN"
                                                                          }
                                                                      };

        public QualityManagementException(ErrorField[] fields) => this.Errors = fields;

        public QualityManagementException(string message, ErrorField[] fields) : base(message) => this.Errors = fields;

        public QualityManagementException(string message, ErrorField[] fields, Exception innerException) : base(message, innerException) =>
            this.Errors = fields;

        public ErrorField[] Errors { get; }
    }
}