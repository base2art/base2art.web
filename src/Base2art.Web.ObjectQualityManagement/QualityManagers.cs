namespace Base2art.Web.ObjectQualityManagement
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Models;

    public static class QualityManagers
    {
        public static async Task ValidateAndVerify<T>(this IQualityManagementLookup lookup, T data)
        {
            lookup.Validate<T>(data);
            await lookup.Verify<T>(data);
        }

        public static void Validate<T>(this IQualityManagementLookup lookup, T data)
        {
            var validators = lookup.GetValidators<T>();

            var resultOf = validators.Select(x => ValidateItem(x, data)).ToArray();

            if (!resultOf.All(x => x.Item1))
            {
                throw new ValidationQualityManagementException(resultOf.SelectMany(x => x.Item2).ToArray());
            }
        }

        public static async Task Verify<T>(this IQualityManagementLookup lookup, T data)
        {
            var validators = lookup.GetVerifiers<T>();

            var resultOf = await Task.WhenAll(validators.Select(x => VerifyItem(x, data)).ToArray());

            if (!resultOf.All(x => x.Item1))
            {
                throw new VerificationQualityManagementException(resultOf.SelectMany(x => x.Item2).ToArray());
            }
        }

        private static async Task<(bool, ErrorField[])> VerifyItem<T>(IVerifier<T> verifier, T data)
        {
            return MapToResult<T>(await verifier.Verify(data));
        }

        private static (bool, ErrorField[]) ValidateItem<T>(IValidator<T> validator, T data)
        {
            return MapToResult<T>(validator.Validate(data));
        }

        private static (bool, ErrorField[]) MapToResult<T>(IQualityManagementResult result)
        {
            if (result == null)
            {
                return (true, new ErrorField[0]);
            }

            return !result.IsValid
                       ? (false, (result.Errors ?? new IQualityManagementError[0]).Where(x => x != null).Select(Map).ToArray())
                       : (true, new ErrorField[0]);
        }

        private static ErrorField Map(IQualityManagementError x)
            => new ErrorField {Code = x.ErrorCode, Message = x.ErrorMessage, Path = x.PropertyName};
    }
}