namespace Base2art.Web.ObjectQualityManagement
{
    using System;
    using Models;

    public class ValidationQualityManagementException : QualityManagementException
    {
        [Obsolete("Use The Errors ctor", true)]
        protected ValidationQualityManagementException() : base()
        {
        }

        public ValidationQualityManagementException(ErrorField[] fields) : base(fields)
        {
        }

        public ValidationQualityManagementException(string message, ErrorField[] fields) : base(message, fields)
        {
        }

        public ValidationQualityManagementException(string message, ErrorField[] fields, Exception innerException)
            : base(message, fields, innerException)
        {
        }
    }
}