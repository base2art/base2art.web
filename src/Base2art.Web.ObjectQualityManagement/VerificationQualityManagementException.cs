namespace Base2art.Web.ObjectQualityManagement
{
    using System;
    using Models;

    public class VerificationQualityManagementException : QualityManagementException
    {
        [Obsolete("Use The Errors ctor", true)]
        protected VerificationQualityManagementException() : base()
        {
        }


        public VerificationQualityManagementException(ErrorField[] fields) : base(fields)
        {
        }

        public VerificationQualityManagementException(string message, ErrorField[] fields) : base(message, fields)
        {
        }

        public VerificationQualityManagementException(string message, ErrorField[] fields, Exception innerException)
            : base(message, fields, innerException)
        {
        }
    }
}