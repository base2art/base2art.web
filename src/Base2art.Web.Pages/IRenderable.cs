namespace Base2art.Web.Pages
{
    using System.Threading.Tasks;

    public interface IRenderable
    {
        Task<string> Render();
    }
}