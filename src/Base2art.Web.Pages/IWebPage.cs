namespace Base2art.Web.Pages
{
    using System.Threading.Tasks;

    public interface IWebPage
    {
        Task<string> ExecuteAsync();
    }
    
    public interface IWebPage<in T>
    {
        Task<string> ExecuteAsync(T model);
    }
}