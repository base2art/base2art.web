namespace Base2art.Web.Pages
{
    using System.Threading.Tasks;

    public class WebPage : IRenderable
    {
        private readonly IRenderable renderable;
        private readonly string contentType;

        public WebPage(IRenderable renderable, string contentType)
        {
            this.renderable = renderable;
            this.contentType = contentType;
        }

        public string ContentType => this.contentType;

        public override string ToString() => this.renderable.Render().Result;
        public Task<string> Render() => this.renderable.Render();

        public static WebPage Create(IWebPage page)
            => Create(page, "text/html");

        public static WebPage Create(IWebPage page, string contentType)
            => new WebPage(new WebPageInternal(page), contentType);

        public static WebPage Create<T>(IWebPage<T> page, T model)
            => Create(page, model, "text/html");

        public static WebPage Create<T>(IWebPage<T> page, T model, string contentType)
            => new WebPage(new WebPageInternal<T>(page, model), contentType);

        private class WebPageInternal<T> : IRenderable
        {
            private readonly IWebPage<T> page;
            private readonly T model;

            public WebPageInternal(IWebPage<T> page, T model)
            {
                this.page = page;
                this.model = model;
            }

            public Task<string> Render()
            {
                return this.page?.ExecuteAsync(this.model) ?? Task.FromResult<string>(null);
            }
        }

        private class WebPageInternal : IRenderable
        {
            private readonly IWebPage page;

            public WebPageInternal(IWebPage page)
            {
                this.page = page;
            }

            public Task<string> Render()
            {
                return this.page?.ExecuteAsync() ?? Task.FromResult<string>(null);
            }
        }
    }
}