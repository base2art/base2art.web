namespace Base2art.Web.ServerEnvironment
{
    public interface IWebServerSettings
    {
        string CurrentDirectory { get; }
        string ContentRoot { get; }
        string WebRoot { get; }
    }
}